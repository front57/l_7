const supRedLine = document.querySelector('.support__redline');
const advRedLine = document.querySelector('.advantages__redline');
const eduRedLine = document.querySelector('.education__redline');


window.addEventListener('resize', setEdRedLineWidth)
window.addEventListener('load', setEdRedLineWidth)
window.addEventListener('resize', setSupRedLine)
window.addEventListener('load', setSupRedLine)
window.addEventListener('resize', setAdvRedLine)
window.addEventListener('load', setAdvRedLine)


function setEdRedLineWidth(){
    if (window.innerWidth > 768){
        const outerWrapper = document.querySelector('.education .content__outer');
        const eduBtn = document.querySelector('.education__btn');
        const eduRow = document.querySelector('.education__row');
        const redLineWidth = eduRow.offsetWidth - eduBtn.offsetWidth + (outerWrapper.offsetWidth - eduRow.offsetWidth) / 2
            - window.getComputedStyle(eduBtn).marginRight.replace('px', '');
        eduRedLine.style.maxWidth = redLineWidth + 'px';
        eduRedLine.style.right = '0';
    }
}
function setSupRedLine(){
    if (window.innerWidth > 992){
        const outerWrapper = document.querySelector('.support .content__outer');
        const supRow = document.querySelector('.support__row');
        const supDesc = document.querySelector('.support__cmp-desc');
        const redLineWidth = supDesc.offsetWidth + (outerWrapper.offsetWidth - supRow.offsetWidth ) / 2;
        supRedLine.style.maxWidth = redLineWidth + 'px';
        supRedLine.style.right = '0';
    }
}

function setAdvRedLine(){
    if(window.innerWidth > 992){
        const outerWrapper = document.querySelector('.advantages .content__outer');
        const details = document.querySelector('.advantages__details');
        const advRow = document.querySelector('.advantages__row');
        advRedLine.style.maxWidth = details.offsetWidth + (outerWrapper.offsetWidth - advRow.offsetWidth) / 2 + 'px';
        advRedLine.style.right = '0';
    }
}
