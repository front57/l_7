const accBtn = document.querySelectorAll('.education__btn');
const panelContent = document.querySelectorAll('.education__panel-wrapper');
const accItem = document.querySelectorAll('.education__accordion-item');

accBtn.forEach(e => e.addEventListener('click', toggleAccordion));
window.addEventListener('resize', activateDefaultTab);
window.addEventListener('load', activateDefaultTab);

function toggleAccordion(){

        const panel = this.nextElementSibling;
        if (window.innerWidth > 768){
                accItem.forEach(e=>e.classList.remove('active'));
                this.parentElement.classList.add('active');
        }else{
                this.parentElement.classList.toggle('active');
        }
        if(panel.style.maxHeight){
                panel.style.maxHeight = null;
        }else{
                panel.style.maxHeight = panel.scrollHeight + 'px';
        }
}

function activateDefaultTab(){
        if (window.innerWidth > 768){
                accItem.forEach((e, idx) => {
                        if (idx !== 0){
                                e.classList.remove('active')
                        }
                    }
                )
                accItem[0].classList.add('active');
        }else{
                accItem.forEach(e => {
                                e.classList.remove('active');
                                panelContent.forEach(e => e.style.maxHeight=null);
                        }
                );
        }
}

