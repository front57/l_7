/*                  MENU                */

/*
window.onload = () => {
    let burger = document.querySelector('.hamburger');
    let header_menu = document.querySelector('.nav__menu');
    let body = document.querySelector('body');
    burger.addEventListener('click',  ()=>{
        burger.classList.toggle('active');
        header_menu.classList.toggle('active');
        body.classList.toggle('lock');
    });
};
*/

/*                  MENU END                */


/*              ACCORDION               */

/*
const acc = document.getElementsByClassName("accordion__header");
const panels = document.querySelectorAll('.accordion__body');

for (let i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
        panels.forEach((e,idx) => {
            if (idx !== i){
                e.style.maxHeight = null;
                e.previousElementSibling.classList.remove('active');
            }else{
                e.style.maxHeight = e.style.maxHeight ? null : e.scrollHeight + "px";
                e.previousElementSibling.classList.toggle('active');
            }
        });
    });
}

*/

/*              ACCORDION END              */


/*                 TABS                    */

/*
window.onload = () => {
    document.getElementById("defaultTab").click();
}

function openTab(ctx, tab) {
    let tabContent = document.getElementsByClassName("tabs__content");
    for (let i = 0; i < tabContent.length; i++) {
        tabContent[i].style.display = "none";
    }
    let tabLinks = document.getElementsByClassName("tabs__link");
    for (let i = 0; i < tabLinks.length; i++) {
        tabLinks[i].classList.remove('active');
    }
    document.getElementById(tab).style.display = "block";
    ctx.classList.add('active');
}
*/

/*              TABS END                */



/*                  MODAL               */

/*
let modal = document.getElementById("myModal");
let modalBtn = document.querySelector('.call-modal');
let modalCls = document.getElementsByClassName("modal__close")[0];

modalBtn.onclick = function() {
    modal.style.display = "block";
}

modalCls.onclick = function() {
    modal.style.display = "none";
}

window.onclick = function(event) {
    if (event.target === modal) {
        modal.style.display = "none";
    }
}
*/

/*                  MODAL END                */

/*                  DOUBLE DOT SLIDER                   */
/*
const sliderOne = document.getElementById('slider-1');
const sliderTwo = document.getElementById('slider-2');

const displayValOne = document.getElementById("range1");
const displayValTwo = document.getElementById("range2");

const sliderField1 = document.querySelector('.slider-input__min');
const sliderField2 = document.querySelector('.slider-input__max');

const sliderTrack = document.querySelector('.slider-track');
let minGap = 5;
let sliderMaxVal = document.getElementById('slider-1').max;


(function (){
    displayValOne.innerHTML = sliderOne.value;
    displayValTwo.innerHTML = sliderTwo.value;
    sliderField1.value = sliderOne.value;
    sliderField2.value = sliderTwo.value;
    fillColor();
})();

function slideOne(){
    if(parseInt(sliderTwo.value) - parseInt(sliderOne.value) <= minGap){
        sliderOne.value = parseInt(sliderTwo.value) - minGap;
    }
    displayValOne.innerHTML = sliderOne.value;
    sliderField1.value = sliderOne.value;
    fillColor();
}

function slideTwo(){
    if(parseInt(sliderTwo.value) - parseInt(sliderOne.value) <= minGap){
        sliderTwo.value = parseInt(sliderOne.value) + minGap;
    }
    displayValTwo.innerHTML = sliderTwo.value;
    sliderField2.value = sliderTwo.value;
    fillColor();
}

function fillColor(){
    let percent1 = (sliderOne.value / sliderMaxVal) * 100;
    let percent2 = (sliderTwo.value / sliderMaxVal) * 100;
    sliderTrack.style.background = `linear-gradient(to right, #ECECEC ${percent1}%, #245462 ${percent1}%,
    #245462 ${percent2}%, #ECECEC ${percent2}%)`;
}

function sliderOneInput(){
    let currentVal = parseInt(sliderField1.value);
    if(typeof currentVal === 'number'){
        sliderOne.value = currentVal;
        if (parseInt(sliderTwo.value) - currentVal <= minGap){
            sliderOne.value = parseInt(sliderTwo.value) - minGap;
        }
        displayValOne.innerHTML = sliderOne.value;
        fillColor();
    }else{
        alert('Value must be a number');
    }
}

function sliderTwoInput(){
    let currentVal = parseInt(sliderField2.value);
    if(typeof currentVal === 'number'){
        sliderTwo.value = currentVal;
        if (currentVal - parseInt(sliderOne.value) <= minGap){
            sliderTwo.value = parseInt(sliderOne.value) + minGap;
        }
        displayValTwo.innerHTML = sliderTwo.value;
        fillColor();
    }else{
        alert('Value must be a number');
    }
}

sliderField1.addEventListener('focusout', () => {
    sliderField1.value = sliderOne.value;
})

sliderField2.addEventListener('focusout', () => {
    sliderField2.value = sliderTwo.value;
})
*/
/*                  DOUBLE DOT SLIDER                   */

/*          CUSTOM DROPDOWN           */
/*

function show(el){
    document.querySelector(".dropdown__input").value = el.textContent;
}

(function(){
    let dropdown = document.querySelector(".dropdown")
    dropdown.addEventListener('click', function(){
        dropdown.classList.toggle("active");
    });
})()

 */
/*          CUSTOM DROPDOWN END           */
