const details = document.querySelector('.advantages__details');
const bgBlock = document.querySelector('.advantages__details-bg');
const outerContent =  document.querySelector('.content__outer');
const bgChoice = document.querySelector('.choice__img');

window.addEventListener('resize', setBgRightImage);
window.addEventListener('load', setBgRightImage);
window.addEventListener('resize', setBgLeftImg);
window.addEventListener('load', setBgLeftImg);

function setBgRightImage(){
    const advRow = document.querySelector('.advantages__row');
    const offset = (outerContent.offsetWidth - advRow.offsetWidth ) / 2;
    if(window.innerWidth > 992){
        window.removeEventListener('resize', setMaxWidth);
        window.removeEventListener('load', setMaxWidth);
        bgBlock.style.maxWidth = details.offsetWidth + offset + 'px';
    }else{
        window.addEventListener('resize', setMaxWidth);
        window.addEventListener('load', setMaxWidth);
    }
    bgBlock.style.maxHeight = document.querySelector('.advantages__details-content').offsetHeight + 'px';
}

function setBgLeftImg(){
    const chHeader = document.querySelector('.choice__header');
    const offset = (outerContent.offsetWidth - chHeader.offsetWidth) / 2;
    const chBlock = document.querySelector('.choice__img-block');
    const chSkills = document.querySelector('.choice__skills');
    if(window.innerWidth > 992){
        bgChoice.style.maxWidth =
            chBlock.offsetWidth + offset + 'px';
    }
    bgChoice.style.maxHeight = chSkills.offsetHeight + 'px';
}

function setMaxWidth(){
    bgBlock.style.maxWidth = '100%';
    bgBlock.style.maxHeight = document.querySelector('.advantages__details-content').offsetHeight + 'px';
}