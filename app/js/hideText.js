const arrBtn = document.querySelector('.support__hide-txt');

arrBtn.addEventListener('click', toggleTxt);

function toggleTxt(){
    this.classList.toggle('active');
    const txtBlock = this.parentElement;
    if (txtBlock.offsetHeight < txtBlock.scrollHeight){
        txtBlock.style.maxHeight = txtBlock.scrollHeight + "px";
    }else{
        txtBlock.style.maxHeight = '12.6em';
    }
}